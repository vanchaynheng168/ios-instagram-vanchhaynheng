//
//  ViewController.swift
//  Instargram
//
//  Created by BTB_011 on 20/11/20.
//

import UIKit

class ViewController: UIViewController {

    let data = [
        [
            "name": "To Love",
            "profile": "girl-anime",
            "post": "girl-anime",
            "profile-bottom": "girl-anime",
            "like": "100k likes",
            "description": "Catch me cat...!",
        ],
        [
            "name": "chay",
            "profile": "boy-anime",
            "post": "boy-anime",
            "profile-bottom": "girl-anime",
            "like": "100k likes",
            "description": "Good night. 9:00am",
        ],
        [
            "name": "vanchhay",
            "profile": "huk",
            "post": "huk",
            "profile-bottom": "girl-anime",
            "like": "200k likes",
            "description": "Our for everyone.",
        ],
        [
            "name": "dara",
            "profile": "iron-man",
            "post": "iron-man",
            "profile-bottom": "girl-anime",
            "like": "300k likes",
            "description": "Today sad nah!",
        ],
        [
            "name": "dona",
            "profile": "thor",
            "post": "thor",
            "profile-bottom": "girl-anime",
            "like": "400k likes",
            "description": "So alone today!",
        ],
        [
            "name": "dona",
            "profile": "thanus",
            "post": "thanus",
            "profile-bottom": "girl-anime",
            "like": "400k likes",
            "description": "So alone today!",
        ],
    ]
    
    
    @IBOutlet weak var storyViewCollection: UICollectionView!

    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        
        let image = UIImage(named: "Logooo")
        navigationItem.titleView  = UIImageView(image: image)
        
        let nibFile = UINib(nibName: "TableViewCell", bundle: nil)
        table.register(nibFile, forCellReuseIdentifier: "cell")
        
        storyViewCollection.register(StoryCollectionViewCell.nib(), forCellWithReuseIdentifier: StoryCollectionViewCell.identifierCell)
    }
}
extension ViewController: UICollectionViewDelegate{
    
}
extension ViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryCollectionViewCell.identifierCell, for: indexPath) as! StoryCollectionViewCell
        let getImage = data[indexPath.row]["post"]!
        let name = data[indexPath.row]["name"]!
        let image = UIImage(named: getImage)!
        cell.configImage(image: image, labelName: name)
        return cell
    }
}
extension ViewController: UICollectionViewDelegateFlowLayout{
    //itemsize
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 120)
    }
}

extension ViewController: UITableViewDelegate{
    
}
extension ViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 425
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let likes = data[indexPath.row]["like"]!
        let description = data[indexPath.row]["description"]!
        let name = data[indexPath.row]["name"]!
        let imagePost = UIImage(named: data[indexPath.row]["post"]!)
        let imageProfile = UIImage(named: data[indexPath.row]["profile"]!)
        let imageBottom = UIImage(named: data[indexPath.row]["profile-bottom"]!)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.config(name: name, likes: likes, description: description, profileTop: imageProfile!, profileBottom: imageBottom!, post: imagePost!)
        return cell
    }
}


