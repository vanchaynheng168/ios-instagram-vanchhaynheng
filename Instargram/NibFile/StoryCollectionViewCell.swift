//
//  StoryCollectionViewCell.swift
//  Instargram
//
//  Created by BTB_011 on 20/11/20.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblNameStory: UILabel!
    @IBOutlet weak var storyView: UIImageView!
    static var identifierCell = "cellCollectionView"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static func nib() -> UINib{
        return UINib(nibName: "StoryCollectionViewCell", bundle: nil)
    }
    public func configImage(image: UIImage, labelName: String){
        storyView?.image = image
        lblNameStory?.text = labelName
    }

}
