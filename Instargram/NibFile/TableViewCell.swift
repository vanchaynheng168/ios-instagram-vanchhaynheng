//
//  TableViewCell.swift
//  Instargram
//
//  Created by Nheng Vanchhay on 20/11/20.
//

import UIKit

class TableViewCell: UITableViewCell, UITextFieldDelegate {

    var liked = true
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var imageProfileButtom: UIImageView!
    @IBOutlet weak var imageProfileTop: UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var txtComment: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func btnComment(_ sender: Any) {
        txtComment.becomeFirstResponder()
    }
    
    @IBAction func viewTapped(_ sender: Any) {
        
        if liked {
            btnLike.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            btnLike.tintColor = .red
            liked = false
        }else{
            btnLike.setImage(UIImage(systemName: "heart"), for: .normal)
            btnLike.tintColor = .systemBlue
            liked = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    public func config(name: String, likes: String, description: String, profileTop: UIImage, profileBottom:UIImage, post: UIImage) {
        self.lblProfileName?.text = name
        self.lblLike?.text = likes
        self.lblDescription?.text = description
        self.imageProfileTop?.image = profileTop
        self.imageProfileButtom?.image = profileBottom
        self.imageCell?.image = post
    }
}
